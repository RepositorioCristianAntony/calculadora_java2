package com.example.calculadorajava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {
    double array[]= new double[10];
    String datos[]= new String[10], operador, resultado;
    int i=1, o=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Button btn0=(Button) findViewById(R.id.btnCero);
        Button btn1=(Button) findViewById(R.id.btnUno);
        Button btn2=(Button) findViewById(R.id.btnDos);
        Button btn3=(Button) findViewById(R.id.btnTres);
        Button btn4=(Button) findViewById(R.id.btnCuatro);
        Button btn5=(Button) findViewById(R.id.btnCinco);
        Button btn6=(Button) findViewById(R.id.btnSeis);
        Button btn7=(Button) findViewById(R.id.btnSiete);
        Button btn8=(Button) findViewById(R.id.btnOcho);
        Button btn9=(Button) findViewById(R.id.btnNueve);

        Button btnPunto=(Button) findViewById(R.id.btnPunto);
        Button btnSuma=(Button) findViewById(R.id.btnMas);
        Button btnResta=(Button) findViewById(R.id.btnMenos);
        Button btnMultiplicar=(Button) findViewById(R.id.btnMulti);
        Button btnDividir=(Button) findViewById(R.id.btnDiv);

        Button btnIgual=(Button) findViewById(R.id.btnIgual);
        Button btnLimpliar=(Button) findViewById(R.id.btnClear);

        TextView txtResultado =(TextView) findViewById(R.id.txtResultado);

        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datos[i]= txtResultado.getText().toString();
                    datos[i]= datos[i]+"0";
                    txtResultado.setText(datos[i]);

            }
        });
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datos[i]= txtResultado.getText().toString();
                    datos[i]= datos[i]+"1";
                    txtResultado.setText(datos[i]);
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datos[i]= txtResultado.getText().toString();
                datos[i]= datos[i]+"2";
                txtResultado.setText(datos[i]);
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datos[i]= txtResultado.getText().toString();
                datos[i]= datos[i]+"3";
                txtResultado.setText(datos[i]);
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datos[i]= txtResultado.getText().toString();
                datos[i]= datos[i]+"4";
                txtResultado.setText(datos[i]);
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datos[i]= txtResultado.getText().toString();
                datos[i]= datos[i]+"5";
                txtResultado.setText(datos[i]);
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datos[i]= txtResultado.getText().toString();
                datos[i]= datos[i]+"6";
                txtResultado.setText(datos[i]);
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datos[i]= txtResultado.getText().toString();
                datos[i]= datos[i]+"7";
                txtResultado.setText(datos[i]);
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datos[i]= txtResultado.getText().toString();
                datos[i]= datos[i]+"8";
                txtResultado.setText(datos[i]);
            }
        });
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datos[i]= txtResultado.getText().toString();
                datos[i]= datos[i]+"9";
                txtResultado.setText(datos[i]);
            }
        });
        btnPunto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datos[i]= txtResultado.getText().toString();
                datos[i]= datos[i]+".";
                txtResultado.setText(datos[i]);
            }
        });


        btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datos[i]= txtResultado.getText().toString();
                array[o]= Double.parseDouble(datos[i]);
                o++;
                operador="+";
                datos[i]="+";
                txtResultado.setText(datos[i]);
                i++;
            }
        });
        btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datos[i]= txtResultado.getText().toString();
                array[o]= Double.parseDouble(datos[i]);
                o=o+1;
                operador="-";
                datos[i]="-";
                txtResultado.setText(datos[i]);
                i=i+1;
            }
        });
        btnMultiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datos[i]= txtResultado.getText().toString();
                array[o]= Double.parseDouble(datos[i]);
                o++;
                operador="*";
                datos[i]="*";
                txtResultado.setText(datos[i]);
                i++;
            }
        });
        btnDividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datos[i]= txtResultado.getText().toString();
                array[o]= Double.parseDouble(datos[i]);
                o++;
                operador="/";
                datos[i]="/";
                txtResultado.setText(datos[i]);
                i++;
            }
        });

        btnIgual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datos[i]= txtResultado.getText().toString();
                array[o]= Double.parseDouble(datos[i]);
                if (operador.equals("+")){
                    resultado= String.valueOf(suma(array[o-1], array[o]));
                    txtResultado.setText(resultado);
                }
                if (operador.equals("-")){
                    resultado= String.valueOf(resta(array[o-1], array[o]));
                    txtResultado.setText(resultado);
                }
                if (operador=="*"){
                    resultado= String.valueOf(multiplicacion(array[o-1], array[o]));
                    txtResultado.setText(resultado);
                }
                if (operador=="/"){
                    resultado= String.valueOf(dividir(array[o-1], array[o]));
                    txtResultado.setText(resultado);
                }
            }
        });
    }
    public double suma(double n1, double n2){
        return n1+n2;
    }
    public  double resta(double n1, double n2){
        return n1-n2;
    }
    public  double multiplicacion(double n1, double n2){
        return n1*n2;
    }
    public  double dividir(double n1, double n2){
        return n1/n2;
    }
}