package com.example.calculadorajava;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    String Numeros, Operador, resultado;
    double num1, num2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn0=(Button) findViewById(R.id.btnCero);
        Button btn1=(Button) findViewById(R.id.btnUno);
        Button btn2=(Button) findViewById(R.id.btnDos);
        Button btn3=(Button) findViewById(R.id.btnTres);
        Button btn4=(Button) findViewById(R.id.btnCuatro);
        Button btn5=(Button) findViewById(R.id.btnCinco);
        Button btn6=(Button) findViewById(R.id.btnSeis);
        Button btn7=(Button) findViewById(R.id.btnSiete);
        Button btn8=(Button) findViewById(R.id.btnOcho);
        Button btn9=(Button) findViewById(R.id.btnNueve);

        Button btnPunto=(Button) findViewById(R.id.btnPunto);
        Button btnSuma=(Button) findViewById(R.id.btnMas);
        Button btnResta=(Button) findViewById(R.id.btnMenos);
        Button btnMultiplicar=(Button) findViewById(R.id.btnMulti);
        Button btnDividir=(Button) findViewById(R.id.btnDiv);

        Button btnIgual=(Button) findViewById(R.id.btnIgual);
        Button btnLimpliar=(Button) findViewById(R.id.btnClear);

        TextView txtResultado =(TextView) findViewById(R.id.txtResultado);

        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Numeros= txtResultado.getText().toString();
            if (Numeros =="+"||Numeros =="-"||Numeros =="*"||Numeros =="/"){
                Numeros="0";
                txtResultado.setText(Numeros);
            }else {
                Numeros= Numeros+"0";
                txtResultado.setText(Numeros);
            }
            }
        });
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numeros= txtResultado.getText().toString();
                if (Numeros =="+"||Numeros =="-"||Numeros =="*"||Numeros =="/"){
                    Numeros="1";
                    txtResultado.setText(Numeros);
                }else {
                    Numeros= Numeros+"1";
                    txtResultado.setText(Numeros);
                }
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numeros= txtResultado.getText().toString();
                if (Numeros =="+"||Numeros =="-"||Numeros =="*"||Numeros =="/"){
                    Numeros="2";
                    txtResultado.setText(Numeros);
                }else {
                    Numeros= Numeros+"2";
                    txtResultado.setText(Numeros);
                }
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numeros= txtResultado.getText().toString();
                if (Numeros =="+"||Numeros =="-"||Numeros =="*"||Numeros =="/"){
                    Numeros="3";
                    txtResultado.setText(Numeros);
                }else {
                    Numeros= Numeros+"3";
                    txtResultado.setText(Numeros);
                }
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numeros= txtResultado.getText().toString();
                if (Numeros =="+"||Numeros =="-"||Numeros =="*"||Numeros =="/"){
                    Numeros="4";
                    txtResultado.setText(Numeros);
                }else {
                    Numeros= Numeros+"4";
                    txtResultado.setText(Numeros);
                }
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numeros= txtResultado.getText().toString();
                if (Numeros =="+"||Numeros =="-"||Numeros =="*"||Numeros =="/"){
                    Numeros="5";
                    txtResultado.setText(Numeros);
                }else {
                    Numeros= Numeros+"5";
                    txtResultado.setText(Numeros);
                }
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numeros= txtResultado.getText().toString();
                if (Numeros =="+"||Numeros =="-"||Numeros =="*"||Numeros =="/"){
                    Numeros="6";
                    txtResultado.setText(Numeros);
                }else {
                    Numeros= Numeros+"6";
                    txtResultado.setText(Numeros);
                }
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numeros= txtResultado.getText().toString();
                if (Numeros =="+"||Numeros =="-"||Numeros =="*"||Numeros =="/"){
                    Numeros="7";
                    txtResultado.setText(Numeros);
                }else {
                    Numeros= Numeros+"7";
                    txtResultado.setText(Numeros);
                }
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numeros= txtResultado.getText().toString();
                if (Numeros =="+"||Numeros =="-"||Numeros =="*"||Numeros =="/"){
                    Numeros="8";
                    txtResultado.setText(Numeros);
                }else {
                    Numeros= Numeros+"8";
                    txtResultado.setText(Numeros);
                }
            }
        });
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numeros= txtResultado.getText().toString();
                if (Numeros =="+"||Numeros =="-"||Numeros =="*"||Numeros =="/"){
                    Numeros="9";
                    txtResultado.setText(Numeros);
                }else {
                    Numeros= Numeros+"9";
                    txtResultado.setText(Numeros);
                }
            }
        });
        btnPunto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numeros= txtResultado.getText().toString();
                if (Numeros =="+"||Numeros =="-"||Numeros =="*"||Numeros =="/"){
                    Numeros=".";
                    txtResultado.setText(Numeros);
                }else {
                    Numeros= Numeros+".";
                    txtResultado.setText(Numeros);
                }
            }
        });
        btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numeros= txtResultado.getText().toString();
                num1= Double.parseDouble(Numeros);
                Numeros="+";
                Operador="+";
                txtResultado.setText(Numeros);
            }
        });
        btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numeros= txtResultado.getText().toString();
                num1= Double.parseDouble(Numeros);
                Numeros="-";
                Operador="-";
                txtResultado.setText(Numeros);
            }
        });
        btnMultiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numeros= txtResultado.getText().toString();
                num1= Double.parseDouble(Numeros);
                Numeros="*";
                Operador="*";
                txtResultado.setText(Numeros);
            }
        });
        btnDividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numeros= txtResultado.getText().toString();
                num1= Double.parseDouble(Numeros);
                Numeros="/";
                Operador="/";
                txtResultado.setText(Numeros);
            }
        });
        btnIgual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numeros= txtResultado.getText().toString();
                num2= Double.parseDouble(Numeros);
                if (Operador=="+"){
                    resultado= String.valueOf(suma(num1, num2));
                    txtResultado.setText(resultado);
                }
                if (Operador=="-"){
                    resultado= String.valueOf(resta(num1, num2));
                    txtResultado.setText(resultado);
                }
                if (Operador=="*"){
                    resultado= String.valueOf(multiplicacion(num1, num2));
                    txtResultado.setText(resultado);
                }
                if (Operador=="/"){
                    resultado= String.valueOf(dividir(num1, num2));
                    txtResultado.setText(resultado);
                }
            }
        });
        btnLimpliar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numeros="";
                txtResultado.setText(Numeros);
                //Intent v2= new Intent(v.getContext(), MainActivity2.class);
                //startActivity(v2);
            }
        });

    }
    public double suma(double n1, double n2){
        return n1+n2;
    }
    public  double resta(double n1, double n2){
        return n1-n2;
    }
    public  double multiplicacion(double n1, double n2){
        return n1*n2;
    }
    public  double dividir(double n1, double n2){
        return n1/n2;
    }

}